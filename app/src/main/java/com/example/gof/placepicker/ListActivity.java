package com.example.gof.placepicker;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, SearchView.OnQueryTextListener {

    public static final String listItemResult = "listItemResult";
    public static final String filterResult = "filterResult";

    private View view;
    private LinearLayout bottomLoadLayout;
    private LinearLayoutManager mLayoutManager;
    private GoogleApiClient mGoogleApiClient;
    private RecyclerView mRecyclerView;
    private PlaceAdapter mAdapter;
    private List<MyPlace> mPlaces;
    private ArrayList<Integer> mFilterArrayList;
    private List<MyPlace> mFilteredPlacesList;
    private ArrayList<String> placeSearchNames;
    private Bitmap mBitmap = null;
    private String searchQuery;
    private Location mLastLocation;

    ArrayList<MyPlace> mainPlaceArray = new ArrayList<>();


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }


        searchQuery = "";

        //Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void updateUI(ArrayList<MyPlace> arrayList) {
        //PlaceSingleton placeSingleton = PlaceSingleton.get(getApplicationContext());
        //List<MyPlace> places = placeSingleton.getPlaces();

        mFilteredPlacesList = new ArrayList<>();


        for (final MyPlace myPlace : arrayList) {

            String myPlaceId = myPlace.getPlaceId();
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, myPlaceId);
            placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(@NonNull PlaceBuffer places) {
                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                        Place place = places.get(0);
                        if (place != null) {
                            myPlace.setPlaceName(place.getName().toString());
                        }
                    }
                    places.release();
                }
            });
            mFilteredPlacesList.add(myPlace);
        }


//        ArrayList<MyPlace> list = new ArrayList<>();
//        list.addAll(mFilteredPlacesList);
        placeSearchNames = new ArrayList<>();
        Log.e("PLACE", "number of places is " + mFilteredPlacesList.size());
        mAdapter = new PlaceAdapter(this, mFilteredPlacesList);
        mAdapter.setHasStableIds(true);
        //mAdapter.replaceAll(mFilteredPlacesList);
        //mAdapter.add(mFilteredPlacesList);
        mRecyclerView.scrollToPosition(0);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


//        if (mAdapter == null) {
//            mAdapter = new PlaceAdapter(mFilteredPlacesList);
//            mAdapter.replaceAll(mFilteredPlacesList);
//            mRecyclerView.setAdapter(mAdapter);
//        } else {
//            mAdapter.notifyDataSetChanged();
//        }
    }


    private class PlaceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTitleTextView;
        public ImageView mImageView;
        public TextView mPlaceFilters;
        public RatingBar mRatingBar;
        public TextView mDistanceTextView;


        public PlaceHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.place_title);
            mImageView = (ImageView) itemView.findViewById(R.id.img_list_item);
            mPlaceFilters = (TextView) itemView.findViewById(R.id.place_filters);
            mRatingBar = (RatingBar) itemView.findViewById(R.id.ratingBar_small);
            mRatingBar.setEnabled(true);
            mRatingBar.setNumStars(5);
            mDistanceTextView = (TextView) itemView.findViewById(R.id.distance);

//            mPriceTextView = (TextView) itemView.findViewById(R.id.place_price);
//            mDistrictTextView = (TextView) itemView.findViewById(R.id.place_district);
//            mTypeTextView = (TextView) itemView.findViewById(R.id.place_type);
        }


        @Override
        public void onClick(View v) {
            //if(!checkNetworkConnection())return;
            if (!isNetworkConnected()) {
                //showNetworkDialog();
                return;
            }

            MyPlace clickedPlace = mPlaces.get(getAdapterPosition());

            Intent i = new Intent(ListActivity.this, ItemActivity.class);
            i.putExtra(listItemResult, clickedPlace);

            startActivity(i);


        }


    }


    private class PlaceAdapter extends RecyclerView.Adapter<PlaceHolder> {

        private Context mContext;


        public PlaceAdapter(Context context, List<MyPlace> places) {
            this.mContext = context;

            mPlaces = new ArrayList<>();
            for (MyPlace myPlace : places) {
                mPlaces.add(myPlace);
            }
//            for(int i = 0; i < 10; i++){
//                if (i >= places.size()) break;
//                mPlaces.add(places.get(i));
//            }
        }

        @Override
        public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View view = layoutInflater
                    .inflate(R.layout.list_item, parent, false);
            return new PlaceHolder(view);
        }

        @Override
        public void onBindViewHolder(final PlaceHolder holder, final int position) {


            final MyPlace myPlace = mPlaces.get(position);

            if (mLastLocation != null) {
                Location location = new Location("A");
                location.setLatitude(myPlace.getLatitude());
                location.setLongitude(myPlace.getLongitude());
                double distance = location.distanceTo(mLastLocation);
                String distanceText = "-";
                if (distance < 1000) {
                    distance = distance * 100;
                    int a = (int) Math.round(distance);
                    distance = (double) a / 100;
                    distanceText = distance + "m";
                } else {
                    distance = distance / 10;
                    int a = (int) Math.round(distance);
                    distance = (double) a / 100;
                    distanceText = distance + "km";
                }
                holder.mDistanceTextView.setText(distanceText);
            }


            String myPlaceId = myPlace.getPlaceId();
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, myPlaceId);
            placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(@NonNull PlaceBuffer places) {
                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                        Place place = places.get(0);
                        if (place != null) {

                            String filters = getResources().getStringArray(R.array.filter_names)[0] +
                                    ": " + getResources().getStringArray(R.array.spinner_array1)[myPlace.getPriceIndex()]
                                    + "\n" + getResources().getStringArray(R.array.filter_names)[1] +
                                    ": " + getResources().getStringArray(R.array.spinner_array2)[myPlace.getDistrictIndex()]
                                    + "\n" + getResources().getStringArray(R.array.filter_names)[2] +
                                    ": " + getResources().getStringArray(R.array.spinner_array3)[myPlace.getTypeIndex()];

                            holder.mPlaceFilters.setText(filters);
                            holder.mTitleTextView.setText(place.getName());
                            holder.mRatingBar.setRating(place.getRating());
                        }
                    }
                    places.release();
                }
            });

            getPhotoForPlace(holder, myPlaceId);


            // Picasso.with(getApplicationContext()).load(mPlace.getAttributions()).resize(100,100).centerCrop().into(holder.mImageView);
        }

        @Override
        public int getItemCount() {
            return (null != mPlaces ? mPlaces.size() : 0);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public void filter(String text) {
            mPlaces.clear();
            if (text.isEmpty()) {
                mPlaces.addAll(mFilteredPlacesList);
            } else {
                text = text.toLowerCase();
                for (MyPlace myPlace : mFilteredPlacesList) {
                    if (myPlace.getPlaceName().toLowerCase().contains(text)) {
                        mPlaces.add(myPlace);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent myIntent = new Intent(ListActivity.this, MainActivity.class);
//        startActivityForResult(myIntent, 0);
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                //onBackPressed();
                getFragmentManager().popBackStack();
                this.onBackPressed();
                break;
            case R.id.action_filter:
                //showFilterDialog();
                Toast.makeText(getApplicationContext(), "Doesn't work!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }


        return true;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        mAdapter.filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {


//        final List<MyPlace> filteredPlaceList = filter(mPlaces, query);
//        searchQuery = query;
//
//        mAdapter.replaceAll(filteredPlaceList);
        //mBinding.recyclerView.scrollToPosition(0);
        searchQuery = query;
        mAdapter.filter(query);
        return true;
    }

    private List<MyPlace> filter(List<MyPlace> places, String query) {
        final String lowerCaseQuery = query.toLowerCase();
//        Log.e("PLACE", "lowerCaseQuery = " + lowerCaseQuery);
//        for (MyPlace place : places) {
//            Log.e("PLACE", "place name = " + place.getPlaceName());
//        }
        final List<MyPlace> placeList = new ArrayList<>();

        for (MyPlace myPlace : places) {
            final String text = myPlace.getPlaceName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                placeList.add(myPlace);
            }
        }
//        Log.e("PLACE", "placeList size " + placeList.size());
        return placeList;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mFilterArrayList != null) {
            outState.putIntegerArrayList(filterResult, mFilterArrayList);
        }

        super.onSaveInstanceState(outState);
    }


    public void getFirebaseData() {


        //showProgressDialog();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("places");
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //HashMap<String,String> hashMap = new HashMap<String, String>();
                //PlaceSingleton placeSingleton = PlaceSingleton.get(this);
                //findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
                mainPlaceArray = new ArrayList<>();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MyPlace place = postSnapshot.getValue(MyPlace.class);
                    place.setUuid(postSnapshot.getKey());
                    //Log.e("PLACE", "lat = " + place.getLatitude() + " , lon = " + place.getLongitude());

                    mainPlaceArray.add(place);


                    //mDatabaseReference.removeEventListener(this);

                    //localList.add(myPlace);
                }


//                Log.e("PLACE", "lat = " + placeSingleton.getPlaces().get(0).getLatitude() + " , lon = " + placeSingleton.getPlaces().get(0).getLongitude());
                updateUI(mainPlaceArray);


//                uploadDatabase(hashMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("DATA", "The read failed: " + databaseError.getMessage());
                //findViewById(R.id.loadingPanel).setVisibility(View.GONE);

            }
        });
    }


    private boolean isNetworkConnected() {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            return (mNetworkInfo == null) ? false : true;

        } catch (NullPointerException e) {
            return false;

        }

    }

//    public boolean checkNetworkConnection() {
//        if (!isNetworkConnected()) {
//            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext()
//                    .getSystemService(LAYOUT_INFLATER_SERVICE);
//            View popupView = layoutInflater.inflate(R.layout.dialog_network, null);
//            final PopupWindow popupWindow = new PopupWindow(popupView,
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    true);
//
//            Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
//            btnDismiss.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    popupWindow.dismiss();
//                }
//            });
//            //popupWindow.showAsDropDown(mButton,mButton.getWidth()/2,-mButton.getHeight());
//            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//            return false;
//        }
//        return true;
//    }

//    public void showNetworkDialog() {
//        DialogNet dialogNet = new DialogNet();
//        dialogNet.show(getFragmentManager(), "dialogNet");
//    }

//    public void showFilterDialog() {
//        DialogFilter dialogFilter = new DialogFilter(this, mFilterArrayList.get(0),
//                mFilterArrayList.get(1), mFilterArrayList.get(2));
//        dialogFilter.show(getSupportFragmentManager(), "dialogFilter");
//    }

//    public void showProgressDialog() {
////        final DialogProgress dialogProgress = new DialogProgress();
////        Thread progressThread = new Thread(new Runnable() {
////            @Override
////            public void run() {
////                dialogProgress.show(getFragmentManager(), "progressBar");
////            }
////        });
////        progressThread.start();
//
//        dialogProgress = new DialogProgress();
//        dialogProgress.show(getFragmentManager(), "progressBar");
//    }

    public void updateFilter(ArrayList<Integer> arrayList) {
        mFilterArrayList = arrayList;
        updateUI(mainPlaceArray);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        PlaceSingleton placeSingleton = PlaceSingleton.get(getContext());
//        updateUI(placeSingleton.getPlaces());
//    }


    @Override
    public void onStart() {
        super.onStart();
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    public void onStop() {
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    public void getPhotoForPlace(final PlaceHolder holder, final String placeId) {
        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {

                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getScaledPhoto(mGoogleApiClient, 200, 200)
                                    .setResultCallback(mDisplayPhotoResultCallback);
                        }
                        photoMetadataBuffer.release();
                    }

                    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
                            = new ResultCallback<PlacePhotoResult>() {
                        @Override
                        public void onResult(PlacePhotoResult placePhotoResult) {
//                                if (!placePhotoResult.getStatus().isSuccess()) {
//                                    return;
//                                }
//                                mBitmap = placePhotoResult.getBitmap();
//                                holder.mImageView.setImageBitmap(mBitmap);

                            mBitmap = placePhotoResult.getBitmap();
                            holder.mImageView.setImageBitmap(mBitmap);

//                            if (!placePhotoResult.getStatus().isSuccess()) {
//                                Log.e("PHOTO", "ResultCallback is not success!");
//                                getPhotoForPlace(holder,placeId);
//                            }

                        }
                    };
                });
    }


    // Initialize the view
    private void init() {

        bottomLoadLayout = (LinearLayout) findViewById(R.id.loadItemsLayout_recyclerView);


        mRecyclerView = (RecyclerView) findViewById(R.id.place_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }


    public void getCurrentLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
        init();
        getFirebaseData();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

}
