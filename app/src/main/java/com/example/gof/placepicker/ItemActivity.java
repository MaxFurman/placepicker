package com.example.gof.placepicker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.example.gof.placepicker.ListActivity.listItemResult;
import static java.security.AccessController.getContext;

public class ItemActivity extends AppCompatActivity {

    protected View view;
    private GoogleApiClient mGoogleApiClient;
    private Place place = null;
    private MyPlace myPlace;
    private RatingBar mRatingBar;
    private Button mMapButton;
    private Button mBackButton;
    private Button mDeleteButton;

    private TextView mPlaceName;
    private TextView mPlaceAddress;
    private TextView mPlaceFilters;
    private TextView mPlaceDetails;

    private String myPlaceId;

    AlertDialog.Builder builder;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_item);
        //setHasOptionsMenu(true);


        myPlace = getIntent().getParcelableExtra(listItemResult);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addApi(AppInvite.API)
                .build();



        mPlaceName = (TextView) findViewById(R.id.place_name);
        mPlaceAddress = (TextView) findViewById(R.id.place_address);
        mPlaceFilters = (TextView) findViewById(R.id.place_filters);
        mPlaceDetails = (TextView) findViewById(R.id.place_details);
        mRatingBar = (RatingBar) findViewById(R.id.rating_Bar);
        mRatingBar.setEnabled(true);
        mRatingBar.setNumStars(5);

        myPlaceId = myPlace.getPlaceId();
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient,myPlaceId);
        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                if(places.getStatus().isSuccess() && places.getCount() > 0){
                    place = places.get(0);
                    if(myPlace != null){
                        //mPlaceName.setText(mPlace.getName());
                        mPlaceName.setText(place.getName());
                        mPlaceAddress.setText(place.getAddress());
                        mRatingBar.setRating(place.getRating());
                    }
                }
                places.release();
            }
        });


        String filters = getResources().getStringArray(R.array.filter_names)[0] +
                ": " + getResources().getStringArray(R.array.spinner_array1)[myPlace.getPriceIndex()]
                + "\n" + getResources().getStringArray(R.array.filter_names)[1] +
                ": " + getResources().getStringArray(R.array.spinner_array2)[myPlace.getDistrictIndex()]
                + "\n" + getResources().getStringArray(R.array.filter_names)[2] +
                ": " + getResources().getStringArray(R.array.spinner_array3)[myPlace.getTypeIndex()];

//        mPlaceName.setText(myPlace.getPlaceName());
//        mPlaceAddress.setText(myPlace.getPlaceAddress());
//        mPlaceFilters.setText(filters);
//        mPlaceDetails.setText(myPlace.getPlaceDetails());
//        mRatingBar.setRating(myPlace.getPlaceRating());



        mPlaceFilters.setText(filters);
        mPlaceDetails.setText(myPlace.getPlaceDetails());





        mBackButton = (Button) findViewById(R.id.backButton);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(ItemActivity.this, ListActivity.class);
//                startActivityForResult(i,0);
//                finish();
                //onBackPressed();
                getFragmentManager().popBackStack();
                onBackPressed();
            }
        });

        mMapButton = (Button) findViewById(R.id.mapButton);

        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!isNetworkConnected()){
//                    showNetworkDialog();
//                    return;
//                }
//                Intent i = new Intent(getActivity(), MapActivity.class);
//                i.putExtra(placeForMap,myPlace);
//                startActivity(i);
                Toast.makeText(getApplicationContext(), "Doesn't work!", Toast.LENGTH_SHORT).show();

            }
        });



        builder = new AlertDialog.Builder(this);
// Add the buttons
        builder.setTitle("Are you sure you want to delete place?");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                Toast.makeText(getApplicationContext(),"OK", Toast.LENGTH_SHORT).show();
                //deletePlace();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
// Set other dialog properties

        mDeleteButton = (Button) findViewById(R.id.deleteButton);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




// Create the AlertDialog
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });





    }

    public void deletePlace(){
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("places");

        mDatabaseReference.child(myPlace.getUuid()).removeValue();

        onBackPressed();
//        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                    MyPlace place = postSnapshot.getValue(MyPlace.class);
//                    place.setUuid(postSnapshot.getKey());
//                    //Log.e("PLACE", "lat = " + place.getLatitude() + " , lon = " + place.getLongitude());
//
//                    if(myPlace.getUuid().equals(place.getUuid())){
//
//                    }
//                    //mDatabaseReference.removeEventListener(this);
//
//                    //localList.add(myPlace);
//                }
//
//
////                Log.e("PLACE", "lat = " + placeSingleton.getPlaces().get(0).getLatitude() + " , lon = " + placeSingleton.getPlaces().get(0).getLongitude());
////                updateUI(mainPlaceArray);
//
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.e("DATA", "The read failed: " + databaseError.getMessage());
//                //findViewById(R.id.loadingPanel).setVisibility(View.GONE);
//
//            }
//        });
    }



    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }



}
