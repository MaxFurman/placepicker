package com.example.gof.placepicker;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    //SkyMall builder.setLatLngBounds(new LatLngBounds(new LatLng(50.4930859,30.559166399999995), new LatLng(50.4930859,30.559166399999995)));
    // Dominos builder.setLatLngBounds(new LatLngBounds(new LatLng(50.439871,30.52417299999999), new LatLng(50.439871,30.52417299999999)));

    int PLACE_PICKER_REQUEST = 1;

    GoogleApiClient mGoogleApiClient;
    FirebaseDatabase database;
    DatabaseReference refId;

    private Spinner mSpinnerPrice;
    private Spinner mSpinnerDistrict;
    private Spinner mSpinnerType;

    private Button mPickButton;
    private Button mAddButton;
    private Button mUpdateButton;
    private Button mListButton;
    private TextView mTextView;
    private EditText mEditText;
    Uri mUri;
    CharSequence number;
    boolean alreadyCreated;


    private int mSpinnerPriceIndex;
    private int mSpinnerDistrictIndex;
    private int mSpinnerTypeIndex;
    private String mPlaceName;
    private String mPlaceAddress;
    private String mDetails;
    private float mRating;
    private double mPlaceLat;
    private double mPlaceLon;
    private String mUuid;

    private List<String> list;
    private String id;
    private String mPlaceId;
    private HashMap<String,String> placesIdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        database = FirebaseDatabase.getInstance();
        refId = database.getReference("places");

        placesIdList = new HashMap<>();
        refId.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot snapshot) {
                placesIdList.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    MyPlace myPlace = postSnapshot.getValue(MyPlace.class);
                    placesIdList.put(postSnapshot.getKey(),myPlace.getPlaceId());

                    // here you can access to name property like university.name

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }


        });



        list = new ArrayList<>();



        mTextView = (TextView) findViewById(R.id.place_name);
        initSpinner(mSpinnerPrice, R.id.spinner1, R.array.spinner_array1);
        initSpinner(mSpinnerDistrict, R.id.spinner2, R.array.spinner_array2);
        initSpinner(mSpinnerType, R.id.spinner3, R.array.spinner_array3);



        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mListButton = (Button) findViewById(R.id.list_button);
        mListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivity(i);
            }
        });

        mPickButton = (Button) findViewById(R.id.pick_button);
        mPickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    alreadyCreated = false;
                    mAddButton.setClickable(true);
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    builder.setLatLngBounds(new LatLngBounds(new LatLng(50.439871,30.52417299999999), new LatLng(50.439871,30.52417299999999)));
                    startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        mAddButton = (Button) findViewById(R.id.add_place);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFirebase();
            }
        });

        mUpdateButton = (Button) findViewById(R.id.update_place);
        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFirebase();
            }
        });




    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                final Place place = PlacePicker.getPlace(data, this);
                Log.e("PLACE",""+place.getLatLng() +":"+place.getId() + ": name = " + place.getName()
                        + ":: rating = " + place.getRating() + ":: types = " + place.getPlaceTypes() +
                        ":: price =  " + place.getPriceLevel() + ":: website = " + place.getWebsiteUri());
                if(refId != null){
                    //refId.setValue(place.getId());
                    mPlaceId = place.getId();
                    mPlaceName = place.getName() + "";
                    mPlaceAddress = place.getAddress() + "";

                    LatLng latLng = place.getLatLng();
                    mPlaceLat = latLng.latitude;
                    mPlaceLon = latLng.longitude;
                    Log.e("PLACE", "latitude = " + mPlaceLat + " , longitude = " + mPlaceLon);

                    Uri uri = place.getWebsiteUri();
                    mUri = uri;

                    number = place.getPhoneNumber();

                    mTextView.setText(mPlaceName + " : " + place.getAddress());

                    final PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient,mPlaceId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if(places.getStatus().isSuccess() && places.getCount() > 0){
                                Place place1 = places.get(0);
                                if(place1 != null){
                                    mRating = place1.getRating();
                                    Log.e("PLACE", "rate =" + mRating);
                                }

                            }
                            places.release();
                        }
                    });

                    mRating = place.getRating();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void createNewPlace(){

        //Check if place already exists
        for(Map.Entry entry: placesIdList.entrySet()){
            if(mPlaceId.equals(entry.getValue())){
                id = String.valueOf(entry.getKey());
                break;
            }
        }

        /**
        * Creating new place
        */
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth

        if (TextUtils.isEmpty(id)) {
            id = refId.push().getKey();
        }

        MyPlace myPlace = new MyPlace(mPlaceId,mPlaceName,mPlaceAddress,mDetails,mUri,number,mRating,
                mPlaceLat,mPlaceLon,
                mSpinnerPriceIndex,mSpinnerDistrictIndex,mSpinnerTypeIndex);

        refId.child(id).setValue(myPlace, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(getApplicationContext(), "Database successfully updated!",Toast.LENGTH_SHORT).show();
            }
        });

        addPlaceChangeListener();


        mSpinnerPriceIndex = 0;
        mSpinnerDistrictIndex = 0;
        mSpinnerTypeIndex = 0;
        mPlaceId = null;
        mRating = 0;
        id = null;
        mPlaceAddress = null;
        mPlaceName = null;
        mDetails = null;

        mUpdateButton.setClickable(false);
        mAddButton.setClickable(true);
        alreadyCreated = true;


        initSpinner(mSpinnerPrice, R.id.spinner1, R.array.spinner_array1);
        initSpinner(mSpinnerDistrict, R.id.spinner2, R.array.spinner_array2);
        initSpinner(mSpinnerType, R.id.spinner3, R.array.spinner_array3);




    }

    /**
     * User data change listener
     */
    private void addPlaceChangeListener() {
        // User data change listener
        refId.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                MyPlace myPlace = dataSnapshot.getValue(MyPlace.class);

                // Check for null
                if (myPlace == null) {
                    Log.e("MYPLACE", "User data is null!");
                    return;
                }

                Log.e("MYPLACE", "User data is changed!" + myPlace.getPlaceId() + ", " + myPlace.getDistrictIndex());

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e("MYPLACE", "Failed to read user", error.toException());
            }
        });
        id = null;
    }

    protected void initSpinner(Spinner mSpinner, int spinId, int array){

        mSpinner = (Spinner) findViewById(spinId);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);


        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()){
                    case R.id.spinner1:
                        mSpinnerPriceIndex = position;
                        break;
                    case R.id.spinner2:
                        mSpinnerDistrictIndex = position;
                        break;
                    case R.id.spinner3:
                        mSpinnerTypeIndex = position;
                        break;
                    default:
                        break;
                }

                //Toast.makeText(getApplicationContext(), parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    public void updateFirebase(){
        if(mPlaceId == null){
            Toast.makeText(getApplicationContext(),"Выберите место!",Toast.LENGTH_SHORT).show();
            return;
        }

        if(mSpinnerPriceIndex == 0 || mSpinnerDistrictIndex == 0 || mSpinnerTypeIndex == 0){
            Toast.makeText(getApplicationContext(),"Выберите все фильтры(Любой - не подходит!)",Toast.LENGTH_SHORT).show();
            return;
        }


        for(Map.Entry entry: placesIdList.entrySet()){
            if(alreadyCreated == true) break;

            if(mPlaceId.equals(entry.getValue())){
                Toast.makeText(getApplicationContext(),"Место уже добавлено,выберите другое или нажмите Update чтобы обновить!",Toast.LENGTH_SHORT).show();
                mAddButton.setClickable(false);
                mUpdateButton.setClickable(true);
                alreadyCreated = true;
                return;
            }
        }




        mTextView.setText("");

        mEditText = (EditText) findViewById(R.id.edit_details);
        mDetails = mEditText.getText().toString();
        mEditText.setText("");


        createNewPlace();
    }
}
